package echo

import (
	"fmt"
)

// Echo - OK this meaningful thing
func Echo() {
	fmt.Println("Hello from echo!")
}
