package main

import (
	"fmt"
	"go_mod_tutorial/echo"
	"github.com/rdegges/go-ipify"
)

func main()  {
	fmt.Println("Hello")
	echo.Echo()
	
	ip, err := ipify.GetIp()
    if err != nil {
        fmt.Println("Couldn't get my IP address:", err)
    } else {
        fmt.Println("My IP address is:", ip)
    }
}
